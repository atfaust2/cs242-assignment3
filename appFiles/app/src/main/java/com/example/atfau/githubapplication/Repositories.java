package com.example.atfau.githubapplication;

import android.app.ListActivity;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONObject;

import static com.example.atfau.githubapplication.MainActivity.getHTML;

public class Repositories extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    static String followersArray = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repositories);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        new Thread(new Runnable() {
            public void run() {
                followersArray = getHTML(MainActivity.followersURL);
            }
        }
        ).start();
        JSONArray jArr;
        String [] profNames = null;
        try {
            while(followersArray == "") {}
            jArr = new JSONArray(followersArray);
            profNames = new String[jArr.length()];
            for(int i = 0; i < jArr.length(); i++) {
                profNames[i] = ((JSONObject)jArr.get(i)).getString("login");
            }
        }
        catch (org.json.JSONException e) {
            System.out.println();
        }
        mAdapter = new MyAdapter(profNames);
        mRecyclerView.setAdapter(mAdapter);



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

}
