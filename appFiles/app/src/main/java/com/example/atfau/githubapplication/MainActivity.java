package com.example.atfau.githubapplication;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

//import com.google.firebase.auth.FirebaseAuth;
//import com.google.firebase.auth.FirebaseUser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

import org.json.*;

public class MainActivity extends AppCompatActivity {

    static String html = "";
    static String av_url = "";
    static Drawable picture = null;
    static String followersURL = "";
    static String followingURL = "";
    static TextView view;
    static String currUser = "pranaygp";
//    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
     //   mAuth = FirebaseAuth.getInstance();

        setup();
        FollowersAdapter.m = this;


    }

    @Override
    public void onStart() {
        super.onStart();

      //  FirebaseUser currentUser = mAuth.getCurrentUser();
    }


    public void setup() {
        html = "";
        TextView name = (TextView) findViewById(R.id.nameText);
        TextView username = (TextView) findViewById(R.id.usernameText);
        TextView bio = (TextView) findViewById(R.id.bioText);
        TextView email = (TextView) findViewById(R.id.emailText);
        TextView numPublicRepos = (TextView) findViewById(R.id.numPublicReposText);
        TextView numFollowers = (TextView) findViewById(R.id.numFollowersText);
        TextView numFollowing = (TextView) findViewById(R.id.numFollowingText);
        ImageView profileImage = findViewById(R.id.ProfileImage);
        TextView createdOn = (TextView) findViewById(R.id.createdText);
        TextView followers = (TextView) findViewById(R.id.FollowersTV);

        GithubUser user = new GithubUser("https://api.github.com/users/" + currUser);

        name.setText(user.realname);
        username.setText(user.username);
        bio.setText(user.bio);
        email.setText(user.email);
        numPublicRepos.setText(Integer.toString(user.numPublicRepos));
        numFollowers.setText(Integer.toString(user.numFollowers));
        numFollowing.setText(Integer.toString(user.numFollowing));
        profileImage.setImageDrawable(picture);
        createdOn.setText(user.createdOn);
        followersURL = user.followersURL;
        followingURL = user.followingURL;

        followers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, FuckMe.class));
            }
        });
    }

    /*
        Found this code snippet on stack overflow
     */
    public static String getHTML(String urlToRead) {
        StringBuilder result = new StringBuilder();
        try {
            URL url = new URL(urlToRead);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            rd.close();
            return result.toString();
        }
        catch (java.net.MalformedURLException e) {
            return "ERROR";
        }
        catch (java.io.IOException e) {
            return "ERROR";
        }
    }

    private static class GithubUser {
        public String createdOn;
        public String username;
        public String realname;
        public String bio;
        public String email;
        public String followersURL;
        public String followingURL;
        public int numPublicRepos;
        public int numFollowers;
        public int numFollowing;
        public Drawable profilePicture;

        public GithubUser(String createdOn, String username, String name, String bio, String website, String email, int numPublicRepos, int numFollowers, int numFollowing) {
            this.createdOn = createdOn;
            this.username = username;
            this.realname = name;
            this.bio = bio;
            this.email = email;
            this.numPublicRepos = numPublicRepos;
            this.numFollowers = numFollowers;
            this.numFollowing = numFollowing;
        }

        public GithubUser(final String url) {
            new Thread(new Runnable() {
                public void run() {
                    html = getHTML(url);
                }
            }
            ).start();

            try {
                while(html.equals("")) { }
                JSONObject obj = new JSONObject(html);
                this.createdOn = obj.getString("created_at");
                this.username = obj.getString("login");
                this.realname = obj.getString("name");
                this.bio = obj.getString("bio");
                this.email = obj.getString("email");
                this.numPublicRepos = obj.getInt("public_repos");
                this.numFollowers = obj.getInt("followers");
                this.numFollowing = obj.getInt("following");
                this.followersURL = obj.getString("followers_url");
                this.followingURL = obj.getString("following_url");
                av_url = obj.getString("avatar_url");
                new Thread(new Runnable() {
                    public void run() {
                        picture = LoadImageFromWebOperations(av_url);
                    }
                }
                ).start();
                while(picture == null) {}
            }
            catch (org.json.JSONException e) {
                System.out.println();
            }
        }

        public static Drawable LoadImageFromWebOperations(String url) {
            try {
                InputStream is = (InputStream) new URL(url).getContent();
                Drawable d = Drawable.createFromStream(is, "src name");
                return d;
            } catch (Exception e) {
                return null;
            }
        }

    }
}