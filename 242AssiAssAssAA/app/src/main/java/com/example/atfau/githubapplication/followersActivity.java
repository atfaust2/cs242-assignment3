package com.example.atfau.githubapplication;

import android.app.ListActivity;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONObject;

import static com.example.atfau.githubapplication.MainActivity.getHTML;

public class followersActivity extends ListActivity {

    static String followersArray = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_followers);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        new Thread(new Runnable() {
            public void run() {
                followersArray = getHTML(MainActivity.followersURL);
            }
        }
        ).start();
        JSONArray jArr;
        String [] profNames = null;
        try {
            while(followersArray == "") {}
            jArr = new JSONArray(followersArray);
            profNames = new String[jArr.length()];
            for(int i = 0; i < jArr.length(); i++) {
                profNames[i] = ((JSONObject)jArr.get(i)).getString("login");
            }
        }
        catch (org.json.JSONException e) {
            System.out.println();
        }

        ListAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, profNames);
        setListAdapter(adapter);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {}

}
