package com.example.atfau.githubapplication;

public class Follower {
    public String username;
    public String imageHtml;

    public Follower(String username, String imageHtml) {
        this.username = username;
        this.imageHtml = imageHtml;
    }

}
